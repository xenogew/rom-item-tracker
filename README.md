# rom-item-tracker

We use `.env` file configuration, create and specify with these values

```
    BOT_TOKEN=***YOUR_BOT_TOKEN_KEY***
    REDIS_URL=***YOUR_REDIS_URL***
```

Or you can setting the deploy instance environment variable with the same name as defined above.
