import { Client, Message } from 'discord.js';
import { inject, injectable } from 'inversify';
import { TYPES } from './types';
import { MessageResponder } from './services/messageResponder';

@injectable()
export class Bot {
    // Code from https://www.toptal.com/typescript/dependency-injection-discord-bot-tutorial
    private client: Client;
    private readonly token: string;
    private messageResponder: MessageResponder;

    constructor(
        @inject(TYPES.Client) client: Client,
        @inject(TYPES.Token) token: string,
        @inject(TYPES.MessageResponder) messageResponder: MessageResponder) {
        this.client = client;
        this.token = token;
        this.messageResponder = messageResponder;

        this.validateConfig(this.token);
    }

    public listen(): Promise<string> {
        this.client.on('ready', () => {
            console.log('Bot has started');
        });

        this.client.on('message', (message: Message) => {
            if (message.author.bot) {
                console.log('Ignoring bot message!');
                return;
            }

            console.log('Message received, content = ', message.content);
            this.messageResponder.handle(message).then(() => {
                console.log('Response sent!');
            }).catch(() => {
                console.log('Response not sent!');
            });
        });

        this.client.on('error', e => {
            console.error('Discord client error!', e);
        });

        return this.client.login(this.token);
    }

    /** Pre-startup validation of the bot config. */
    private validateConfig(token: string) {
        if (!token) {
            throw new Error('You need to specify your Discord bot token!');
        }
    }

}
