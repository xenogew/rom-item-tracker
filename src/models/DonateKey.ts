export class DonateKey {
    key: string;
    alias: Array<string>;
    type: string;
    tier: number;
    price?: number;
    increment?: Array<number>;
}
