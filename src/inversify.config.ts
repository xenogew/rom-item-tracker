import 'reflect-metadata';
import { Container } from 'inversify';
import { TYPES } from './types';
import { Bot } from './bot';
import { Client } from 'discord.js';
import { MessageResponder } from './services/messageResponder';
import { PingFinder } from './services/pingFinder';
import { CommandsManager } from './services/commandsManager';
import Axios from 'axios';
import * as IORedis from 'ioredis';
import * as JunkKeys from './resources/junk_list.json';
import * as DonateKeys from './resources/donate_list.json';
import { table as AsciiTable } from 'table';
import { DonateKey } from './models/DonateKey';

const container = new Container();

container.bind<Bot>(TYPES.Bot).to(Bot).inSingletonScope();
container.bind<Client>(TYPES.Client).toConstantValue(new Client());
container.bind<string>(TYPES.Token).toConstantValue(process.env.BOT_TOKEN);
container.bind<MessageResponder>(TYPES.MessageResponder).to(MessageResponder).inSingletonScope();
container.bind<PingFinder>(TYPES.PingFinder).to(PingFinder).inSingletonScope();
container.bind<CommandsManager>(TYPES.CommandsManager).toConstantValue(new CommandsManager('!'));
container.bind<any>(TYPES.Axios).toConstantValue(
    Axios.create(
        { baseURL: 'https://api.poporing.life/',
          timeout: 0,
          headers: {Origin: 'https://poporing.life'}}));
container.bind<any>(TYPES.IoRedis).toConstantValue(new IORedis(process.env.REDIS_URL));
container.bind<Array<string>>(TYPES.JunkItems).toConstantValue(JunkKeys.list);
container.bind<Array<DonateKey>>(TYPES.DonateItems).toConstantValue(DonateKeys.list);
container.bind<any>(TYPES.AsciiTable).toConstantValue(AsciiTable);

export default container;
