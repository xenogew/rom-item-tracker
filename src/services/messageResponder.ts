import { Message } from 'discord.js';
import { PingFinder } from './pingFinder';
import { inject, injectable } from 'inversify';
import { TYPES } from '../types';
import { CommandsManager } from './commandsManager';

@injectable()
export class MessageResponder {
    private pingFinder: PingFinder;
    private commandsManager: CommandsManager;

    constructor(
        @inject(TYPES.PingFinder) pingFinder: PingFinder,
        @inject(TYPES.CommandsManager) commandsManager: CommandsManager
    ) {
        this.pingFinder = pingFinder;
        this.commandsManager = commandsManager;
    }

    async handle(message: Message): Promise<Message | Message[]> {
        console.log('handle message : ', message.content);
        if (this.pingFinder.isPing(message.content)) {
            return message.reply('pong!');
        }
        console.log('it act as command? => ', this.commandsManager.isCommand(message.content));
        if (this.commandsManager.isCommand(message.content)) {
            await this.commandsManager.handleMessage(message)
            .then(() => {
                console.log('success on command performed');
            }).catch(() => {
                console.log('fail on command perform');
            });
        }

        return Promise.reject();
    }
}
