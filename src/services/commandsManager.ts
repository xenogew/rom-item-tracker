import { injectable } from 'inversify';
import { listCommands } from '../commands/listCommands';
import { GreetCommand } from '../commands/greet';
import { Command } from '../commands/infCommand';
import { Message } from 'discord.js';
import { CommandContext } from '../models/commandContext';
import { reactor } from '../reactions/reactor';
import { JunkCommand } from '../commands/junk';
import { DonateCommand } from '../commands/donate';
import { HelpCommand } from '../commands/help';
import { EndlessTowerCommand } from '../commands/endlessTower';
import { CheapCardCommand } from '../commands/cheapCard';

@injectable()
export class CommandsManager {
    listCommands = listCommands;
    commands: Command[];

    private readonly prefix: string;

    constructor(prefix: string) {
        const commandClasses = [
            GreetCommand,
            JunkCommand,
            DonateCommand,
            EndlessTowerCommand,
            CheapCardCommand,
        ];

        this.commands = commandClasses.map(commandClass => new commandClass());
        this.commands.push(new HelpCommand(this.commands));
        this.prefix = prefix;
    }

    public isCommand(stringToSearch: string): boolean {
        console.log('check is it command, the prefix settings = ', this.prefix);
        return stringToSearch.startsWith(this.prefix);
    }

    /** Executes user commands contained in a message if appropriate. */
    async handleMessage(message: Message): Promise<void> {
        if (message.author.bot) {
            return;
        }

        const commandContext = new CommandContext(message, this.prefix);

        const allowedCommands = this.commands.filter(command => command.hasPermissionToRun(commandContext));
        const matchedCommand = this.commands.find(command => command.commandNames.includes(commandContext.parsedCommandName));

        if (!matchedCommand) {
            await message.reply(`I don't recognize that command. Try !help.`);
            await reactor.failure(message);
        } else if (!allowedCommands.includes(matchedCommand)) {
            await message.reply(`you aren't allowed to use that command. Try !help ${matchedCommand.commandNames[0]}.`);
            await reactor.failure(message);
        } else {
            console.log('Command context found and matched', matchedCommand.commandNames);
            await matchedCommand.run(commandContext).then(() => {
                reactor.success(message);
            }).catch(() => {
                reactor.failure(message);
            });
        }
    }
}
