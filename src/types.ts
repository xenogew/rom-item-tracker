export const TYPES = {
    Bot: Symbol('Bot'),
    Client: Symbol('Client'),
    Token: Symbol('Token'),
    MessageResponder: Symbol('MessageResponder'),
    PingFinder: Symbol('PingFinder'),
    CommandsManager: Symbol('CommandsManager'),
    Axios: Symbol('Axios'),
    IoRedis: Symbol('IoRedis'),
    JunkItems: Symbol('JunkItems'),
    DonateItems: Symbol('DonateItems'),
    AsciiTable: Symbol('AsciiTable'),
};
