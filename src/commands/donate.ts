import { injectable, inject } from 'inversify';
import 'reflect-metadata';
import { Command } from './infCommand';
import { CommandContext } from '../models/commandContext';
import { NumbersUtils } from '../utils/numbers';
import { TYPES } from '../types';
import { to } from 'await-to-js';
import Axios, { AxiosInstance } from 'axios';
import * as DonateKeys from '../resources/donate_list.json';
import * as IORedis from 'ioredis';
import { table as AsciiTable } from 'table';
import { DonateKey } from '../models/DonateKey';

@injectable()
export class DonateCommand implements Command {
    commandNames = ['donate', 'd', 'donation'];
    @inject(TYPES.DonateItems) private donateKeys: Array<DonateKey>;
    @inject(TYPES.IoRedis) private redisClient: any;
    @inject(TYPES.Axios) private axios: AxiosInstance;
    @inject(TYPES.AsciiTable) private asciiTable: any;

    config = {
        columns: {
            0: {
                alignment: 'right'
            },
            1: {
                alignment: 'right'
            },
            2: {
                alignment: 'right'
            },
            3: {
                alignment: 'right'
            },
            4: {
                alignment: 'right'
            },
        }
    };

    constructor() {
        this.donateKeys = this.donateKeys ? this.donateKeys : DonateKeys.list;
        this.redisClient = this.redisClient ? this.redisClient : new IORedis(process.env.REDIS_URL);
        this.axios = this.axios ? this.axios : Axios.create({
            baseURL: 'https://api.poporing.life/',
            timeout: 0,
            headers: {Origin: 'https://poporing.life'}});
        this.asciiTable = this.asciiTable ? this.asciiTable : AsciiTable;
    }

    getHelpMessage(commandPrefix: string): string {
        let itemListingResponseMsg = '**Items List can use:**\n\n```';
        this.donateKeys.forEach(dKey => {
            itemListingResponseMsg += (`    ${dKey.key.replace(/_/g, ' ')},\n`);
        });
        itemListingResponseMsg += '```';
        return `Use ${commandPrefix}${this.commandNames[0]} to check your specific guild donation items as summary table.
\n\`${commandPrefix}${this.commandNames[0]} [args: Item_Name]\`, Item_Name is required.
\n\n${itemListingResponseMsg}`;
    }

    async run(parsedUserCommand: CommandContext): Promise<void> {
        const msg = parsedUserCommand.originalMessage;
        const args = parsedUserCommand.args;
        console.log(args, ', ', args.length);
        if (!args || args.length === 0) {
            await msg.reply(this.getHelpMessage(parsedUserCommand.commandPrefix));
            return;
        }

        let donateObj: any;
        const argItem: DonateKey = this.__findKeyFromAlias(
            args
            .filter(arg => !arg.startsWith('--'))
            .map(arg => arg.toLowerCase())
            .join(' '));
        if (argItem === null) {
            await msg.reply(`Apologize bros, I can't find **${args.join(' ')}** :sob:`);
            return;
        }

        let startOption: number;
        if (args.map(arg => arg.toLowerCase()).find(arg => arg.startsWith('--start='))) {
            startOption = parseInt(args
                .map(arg => arg.toLowerCase())
                .filter(arg => arg.startsWith('--start='))[0].split('=')[1], 10);
        } else {
            startOption = 30;
        }
        startOption = [25, 30, 35, 42].includes(startOption) ? startOption : 30;

        let incremental: Array<number>;
        let npcItemPrice = 0;
        if (argItem.type === 'normal') {
            incremental = DonateKeys.incr.filter(ary => ary[0] === startOption)[0];
        } else if (argItem.type === 'emperium') {
            incremental = [1];
        } else if (argItem.type === 'boss') {
            incremental = [1, 2, 3, 4, 5];
        } else if (argItem.type === 'npc' || argItem.type === 'event') {
            incremental = argItem.increment;
            npcItemPrice = argItem.price;
        } else {
            // Default incremental list
            incremental = [30, 42, 60, 90, 120];
        }
        console.log('args =', `${args} => ---${argItem.key}---`);

        const [errRedis, donateItemCache] = await to(this.redisClient.get(`donate_cache_${argItem.key}`));
        console.log('Response of Redis : ', donateItemCache);
        if (argItem.type === 'npc' || argItem.type === 'event') {
            donateObj = {
                data: {
                    item_name: argItem.key,
                    data: {
                        price: argItem.price,
                        type: argItem.type
                    }
                }
            };
        } else if (errRedis ||
            donateItemCache === null ||
            donateItemCache === undefined ||
            donateItemCache.length === 0
        ) {
            console.log('Not found data in Cache');

            const [errAxios, axiosResponse] = await to(this.axios.get(`/get_latest_price/${argItem.key}`));

            if (errAxios || !axiosResponse) {
                await msg.reply('Found error on fetching from Poporing Life');
                return;
            }

            donateObj = axiosResponse.data;
            this.redisClient.set(`donate_cache_${argItem.key}`, JSON.stringify(donateObj), 'EX', 3600);
        } else {
            donateObj = JSON.parse(donateItemCache);
        }
        await msg.channel.send(this.__msgBuild(donateObj, incremental));
    }

    hasPermissionToRun(_: CommandContext): boolean {
        return true;
    }

    private __msgBuild(donateResponseData: any, incrementList: Array<number>): any {
        donateResponseData = donateResponseData.data;
        const itemName = donateResponseData.item_name;

        const unitPrice = donateResponseData.data.price;
        // [#, donate price, total price]
        const tbHeader = ['#', 'Donate', 'Total'];
        const donationPcs = incrementList;
        const donationTbl = [];
        donationTbl.push(tbHeader);
        let totalSumPrice = 0;
        const rowDntNpc = [['##', 'Merchant', 'Net.DC']];
        for (const donationPc of donationPcs) {
            totalSumPrice += (donationPc * unitPrice);
            const rowDonate = [donationPc,
                NumbersUtils.formatNumber(donationPc * unitPrice),
                NumbersUtils.formatNumber(totalSumPrice)];
            if (donateResponseData.data.type && donateResponseData.data.type === 'npc') {
                rowDntNpc.push(['->',
                    NumbersUtils.formatNumber(donationPc * unitPrice * 0.9),
                    NumbersUtils.formatNumber(totalSumPrice * 0.9)]);
            }
            donationTbl.push(rowDonate);
        }
        if (donateResponseData.data.type && donateResponseData.data.type === 'npc') {
            rowDntNpc.forEach(rowNpc => {
                donationTbl.push(rowNpc);
            });
        }

        // calculate psc * prices and use `table` to display in full-width text use the code markdown pattern
        const buildMsg = {embed: {
            title: `${itemName.replace(/_/g, ' ').replace(/(?:^|\s)\S/g, (c1: string) => c1.toUpperCase())}`,
            url: `https://poporing.life/?search=:${itemName}`, // titleプロパティのテキストに紐付けられるURL
            description: '\nUnit Price: ' + NumbersUtils.formatNumber(unitPrice) + 'z\n```'
                + this.asciiTable(donationTbl, this.config) +  '```\n\n'
                + (incrementList[0] !== 1 && !(donateResponseData.data.type && donateResponseData.data.type === 'npc') ?
                    `**Note:** Use \`!d <item_name> --start=XX\`, XX are in [25, 30, 35, 42] only available,\n
If use only \`!d <item_name>\` then \`--start=30\` will be applied.` :
                    ''
                  )
                + (donateResponseData.data.type && donateResponseData.data.type === 'npc' ?
                    '**Note:** Calculate discount only DC. max 10%' : ''),
            color: 7506394,
            timestamp: new Date(),
            thumbnail: {
                url: `https://rom-item-tracker-bot.firebaseapp.com/assets/images/items/${itemName.replace(/_/g, '-')}.png`
            },
        }};
        console.log(buildMsg);

        return buildMsg;
    }

    private __findKeyFromAlias(args: string): DonateKey {
        console.log('args=>', args, ', Donate Keys=>', this.donateKeys);
        for (const dnk of this.donateKeys) {
            if (dnk.alias.includes(args) || dnk.key === args
              || (args.length >= 3 && dnk.key.startsWith(args))) {
                return dnk;
            }
        }
        return null;
    }
}
