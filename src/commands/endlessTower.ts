import { injectable, inject } from 'inversify';
import 'reflect-metadata';
import { Command } from './infCommand';
import { CommandContext } from '../models/commandContext';
import { TYPES } from '../types';
import { to } from 'await-to-js';
import Axios, { AxiosInstance } from 'axios';
import * as IORedis from 'ioredis';
import { table as AsciiTable } from 'table';
import * as moment from 'moment';
import * as urlExist from 'url-exists-async-await';

@injectable()
export class EndlessTowerCommand implements Command {
    commandNames = ['et', 'endless-tower'];
    @inject(TYPES.IoRedis) private redisClient: any;
    @inject(TYPES.Axios) private axios: AxiosInstance;
    @inject(TYPES.AsciiTable) private asciiTable: any;
    ENDLESS_TOWER_REDIS_KEY = 'endless_tower';
    cacheMessage =
`Hello! Looking for **__Updated ET LIST__**?

**__SEA Endless Tower ##START_OF_WEEK## - ##END_OF_WEEK## Boss List__**
MVP LIST: ##MVP_LIST_URL##
MINI LIST: ##MINI_LIST_URL##

List will be reset at __##EXPIRE_AT##__`;
    startOfWeek: any;
    endOfWeek: any;
    expireAt: any;

    constructor() {
        this.redisClient = this.redisClient ? this.redisClient : new IORedis(process.env.REDIS_URL);
        this.axios = this.axios ? this.axios : Axios.create({
            baseURL: 'https://api.poporing.life/',
            timeout: 0,
            headers: {Origin: 'https://poporing.life'}});
        this.asciiTable = this.asciiTable ? this.asciiTable : AsciiTable;
        // Initial common datetime (moment)
        // Prefer to GMT+8 of Malaysian time, because of our guild members
        this.startOfWeek = moment().utcOffset(8).startOf('isoWeek');
        this.endOfWeek = moment().utcOffset(8).endOf('isoWeek');
        // Expiry date/time will be next Monday, add 21601s = 6 hours 1 second
        this.expireAt = moment().utcOffset(8).endOf('isoWeek').add(21601, 'second');
    }

    getHelpMessage(commandPrefix: string): string {
        let endlessTowerHelpResponseMsg = '**Options:**\n';

        endlessTowerHelpResponseMsg += '\`--mvp=URL\` to set the Image URL of MVP list\n';
        endlessTowerHelpResponseMsg += '\`--mini=URL\` to set the Image URL of MINI list\n';
        endlessTowerHelpResponseMsg += 'Only allow member who has role **\'Admin\' and UP** can call the options command';
        return `Use \`${commandPrefix}${this.commandNames[0]}\` to check current week MVP/MINI list of each channel.
\n\`${commandPrefix}${this.commandNames[0]} [--mvp=URL] [--mini=URL]\`
\n${endlessTowerHelpResponseMsg}`;
    }

    async run(parsedUserCommand: CommandContext): Promise<void> {
        const msg = parsedUserCommand.originalMessage;
        const args = parsedUserCommand.args.map(arg => {
            const argSplit = arg.split('=');
            return `${argSplit[0].toLocaleLowerCase()}=${argSplit[1]}`;
        });
        if (!args || args.length === 0) {
            const responseMsg = await this.__msgBuild();
            await msg.reply(responseMsg);
            return;
        } else if (args.find(arg => arg.startsWith('--mvp=') || arg.startsWith('--mini='))) {
            if (!this.hasPermissionToRun(parsedUserCommand)) {
                await msg.reply(', Hey please bro, you are not **\'Admin\'** :scream:\n Ask your Admin members for updating message.');
                return;
            }
            // Find out command input as args passing into command
            const mvpArg = args.find(arg => arg.startsWith('--mvp='));
            console.log(mvpArg.substring('--mvp='.length, mvpArg.length));
            let mvpUrl = mvpArg ? mvpArg.substring('--mvp='.length, mvpArg.length) : '';
            const mvpUrlExist = await urlExist(mvpUrl);
            mvpUrl = mvpUrlExist ? mvpUrl : '';
            const miniArg = args.find(arg => arg.startsWith('--mini='));
            let miniUrl = miniArg ? miniArg.substring('--mini='.length, miniArg.length) : '';
            console.log(miniArg.substring('--mini='.length, miniArg.length));
            const miniUrlExist = await urlExist(miniUrl);
            miniUrl = miniUrlExist ? miniUrl : '';
            // Set the Redis cache after check input
            await this.__setCacheMessage(mvpUrl, miniUrl);
            await msg.reply(`MVP is \`${mvpUrl ? mvpUrl.split('://')[1] : 'not set'}\`, ` +
                            `MINI is \`${miniUrl ? miniUrl.split('://')[1] : 'not set'}\``);
            return;
        } else {
            await msg.reply(this.getHelpMessage(parsedUserCommand.commandPrefix));
            return;
        }
    }

    hasPermissionToRun(parsedUserCommand: CommandContext): boolean {
        const args = parsedUserCommand.args.map(arg => arg.toLowerCase());
        const member = parsedUserCommand.originalMessage.member;
        member.roles.find(role => {
            console.log('We try to find, are you a Admin member ===> ', role.name);
            return role.name.startsWith('Admin');
        });
        if (!args || args.length === 0) {
            return true;
        } else if (args.find(arg => arg.startsWith('--mvp=') || arg.startsWith('--mini='))
            && member.roles.find(role => role.name.startsWith('Admin'))) {
            return true;
        } else {
            return false;
        }
    }

    private async __msgBuild(): Promise<string> {
        let buildMsg: string;
        const [errRedis, redisCacheMsg] = await to(this.redisClient.get(this.ENDLESS_TOWER_REDIS_KEY));
        if (errRedis || redisCacheMsg === null) {
            buildMsg = 'Not yet update on this week. :dizzy_face:';
        } else {
            // The message doesn't has update list, replace with empty string
            buildMsg = redisCacheMsg.replace('##MVP_LIST_URL##', '')
                                    .replace('##MINI_LIST_URL##', '');
        }
        return buildMsg;
    }

    private async __setCacheMessage(mvpUrl: string, miniUrl: string) {
        this.cacheMessage = mvpUrl !== '' ? this.cacheMessage.replace('##MVP_LIST_URL##', mvpUrl) : this.cacheMessage;
        this.cacheMessage = miniUrl !== '' ? this.cacheMessage.replace('##MINI_LIST_URL##', miniUrl) : this.cacheMessage;
        this.cacheMessage = this.cacheMessage
                                .replace('##EXPIRE_AT##', this.expireAt.toString())
                                .replace('##START_OF_WEEK##', this.startOfWeek.format('Do MMM'))
                                .replace('##END_OF_WEEK##', this.endOfWeek.format('Do MMM'));
        console.log(this.expireAt.unix() - moment().utcOffset(8).unix());
        await this.redisClient.set(this.ENDLESS_TOWER_REDIS_KEY, this.cacheMessage,
            'EX', this.expireAt.unix() - moment().utcOffset(8).unix());
    }
}
