import { injectable, inject } from 'inversify';
import 'reflect-metadata';
import { Command } from './infCommand';
import { CommandContext } from '../models/commandContext';
import { NumbersUtils } from '../utils/numbers';
import { TYPES } from '../types';
import { to } from 'await-to-js';
import Axios, { AxiosInstance } from 'axios';
import * as JunkKeys from '../resources/junk_list.json';
import * as IORedis from 'ioredis';

@injectable()
export class JunkCommand implements Command {
    commandNames = ['junk', 'junks', 'j'];
    @inject(TYPES.JunkItems) private junkKeys: Array<string>;
    @inject(TYPES.IoRedis) private redisClient: any;
    @inject(TYPES.Axios) private axios: AxiosInstance;

    constructor() {
        this.junkKeys = this.junkKeys ? this.junkKeys : JunkKeys.list;
        this.redisClient = this.redisClient ? this.redisClient : new IORedis(process.env.REDIS_URL);
        this.axios = this.axios ? this.axios : Axios.create({
            baseURL: 'https://api.poporing.life/',
            timeout: 0,
            headers: {Origin: 'https://poporing.life'}});
    }

    getHelpMessage(commandPrefix: string): string {
        return `Use ${commandPrefix}${this.commandNames[0]} to check all normal loots from mobs and prices in Exchanges market`;
    }

    async run(parsedUserCommand: CommandContext): Promise<void> {
        const msg = parsedUserCommand.originalMessage;
        let junkObj: any;

        let replyMsg = '\n';
        const [errRedis, junkCache] = await to(this.redisClient.get('junk_cache'));
        console.log('Response of Redis : string.length', junkCache ? junkCache.length : 0);
        if (errRedis ||
            junkCache === null ||
            junkCache === undefined ||
            junkCache.length === 0
        ) {
            console.log('Not found data in cache');
            replyMsg += 'From Poporinglife\n';

            const [errAxios, axiosResponse] = await to(this.axios.post('/get_latest_prices', this.junkKeys));

            if (errAxios || !axiosResponse) {
                await msg.reply('Found error on fetching from Poporing Life');
                return;
            }

            junkObj = axiosResponse.data.data;
            this.redisClient.set('junk_cache', JSON.stringify(junkObj), 'EX', 10800);
        } else {
            replyMsg += 'From RedisCache\n';
            junkObj = JSON.parse(junkCache);
        }
        replyMsg += this.__msgBuild(junkObj);
        await msg.reply(replyMsg);
    }

    hasPermissionToRun(parsedUserCommand: CommandContext): boolean {
        return true;
    }

    private __msgBuild(junkResponseData: Array<any>): string {
        let buildMsg = '';
        for (let i = 1; i <= junkResponseData.length; i++) {
          const junk = junkResponseData[i - 1];
          buildMsg +=
            i +
            '. **' +
            junk.item_name.replace(/_/g, ' ').replace(/(?:^|\s)\S/g, (c1: string) => c1.toUpperCase()) +
            '** price: ' +
            NumbersUtils.formatNumber(junk.data.price) +
            'z (' +
            NumbersUtils.formatISOAbbr(junk.data.volume, 1) +
            ' ea.)';
          buildMsg += i === junkResponseData.length ? '' : '\n';
        }

        return buildMsg;
    }
}
