import { injectable, inject } from 'inversify';
import 'reflect-metadata';
import { Command } from './infCommand';
import { CommandContext } from '../models/commandContext';
import { TYPES } from '../types';
import { to } from 'await-to-js';
import Axios, { AxiosInstance } from 'axios';
import * as IORedis from 'ioredis';
import { table as AsciiTable } from 'table';

@injectable()
export class CheapCardCommand implements Command {
    commandNames = ['ccd', 'cheap-card'];
    @inject(TYPES.IoRedis) private redisClient: any;
    @inject(TYPES.Axios) private axios: AxiosInstance;
    @inject(TYPES.AsciiTable) private asciiTable: any;

    constructor() {
        this.redisClient = this.redisClient ? this.redisClient : new IORedis(process.env.REDIS_URL);
        this.axios = this.axios ? this.axios : Axios.create({
            baseURL: 'https://api.poporing.life/',
            timeout: 0,
            headers: {Origin: 'https://poporing.life'}});
        this.asciiTable = this.asciiTable ? this.asciiTable : AsciiTable;
    }

    getHelpMessage(commandPrefix: string): string {
        let endlessTowerHelpResponseMsg = '**Options:**\n';

        endlessTowerHelpResponseMsg += '\`--mvp=URL\` to set the Image URL of MVP list\n';
        endlessTowerHelpResponseMsg += '\`--mini=URL\` to set the Image URL of MINI list\n';
        endlessTowerHelpResponseMsg += 'Only allow member who has role **\'Duke\' and UP** can call the options command';
        return `Use \`${commandPrefix}${this.commandNames[0]}\` to check current week MVP/MINI list of each channel.
\n\`${commandPrefix}${this.commandNames[0]} [--mvp=URL] [--mini=URL]\`
\n${endlessTowerHelpResponseMsg}`;
    }

    async run(parsedUserCommand: CommandContext): Promise<void> {
        const msg = parsedUserCommand.originalMessage;
        const args = parsedUserCommand.args.map(arg => {
            const argSplit = arg.split('=');
            return `${argSplit[0].toLocaleLowerCase()}=${argSplit[1]}`;
        });
        if (!args || args.length === 0) {
            const responseMsg = await this.__msgBuild();
            await msg.reply(responseMsg);
            return;
        } else if (args.find(arg => arg.startsWith('--mvp=') || arg.startsWith('--mini='))) {
            if (!this.hasPermissionToRun(parsedUserCommand)) {
                await msg.reply(', Hey please bro, you are not **\'Duke\'** :scream:\n Ask your Duke members for updating message.');
                return;
            }
            // Find out command input as args passing into command
            return;
        } else {
            await msg.reply(this.getHelpMessage(parsedUserCommand.commandPrefix));
            return;
        }
    }

    hasPermissionToRun(parsedUserCommand: CommandContext): boolean {
        const args = parsedUserCommand.args.map(arg => arg.toLowerCase());
        const member = parsedUserCommand.originalMessage.member;

        if (!args || args.length === 0) {
            return true;
        } else {
            return false;
        }
    }

    private async __msgBuild(): Promise<string> {
        let buildMsg: string;
        buildMsg = '';

        return buildMsg;
    }

}
