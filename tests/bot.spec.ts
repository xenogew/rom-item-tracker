import { Client } from 'discord.js';
import { mock, instance } from 'ts-mockito';
import 'chai';

describe('Bot', () => {
    let discordMock: Client;
    let discordInstance: Client;

    beforeEach(() => {
        discordMock = mock(Client);
        discordInstance = instance(discordMock);
    });

});
